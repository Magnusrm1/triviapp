import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
// component imports.
import TriviaQuestion from './components/TriviaQuestion';
import TriviaResult from './components/TriviaResult';
import TriviaStart from './components/TriviaStart';

Vue.use(VueRouter);

Vue.config.productionTip = false;



const routes = [
  { path: '/question:id', component: TriviaQuestion},
  { path: '/results', component: TriviaResult},
  { path: '/', component: TriviaStart}
];

const router = new VueRouter({ routes });

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
