# trivi-app


## Planning
* [x] import routing, vuex
* [x] frontpage with startbutton
* [x] question view based on id
* [x] question view should have navigation to next (prev if time).
* [x] results page, show score, restart button, compare answers to correct..


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
