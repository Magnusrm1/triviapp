export const data = {
    "response_code": 0,
    "results": [
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which video game earned music composer Mike Morasky the most awards for his work?",
            "correct_answer": "Portal 2",
            "incorrect_answers": [
                "Left 4 Dead 2",
                "Team Fortress 2",
                "Counter-Strike: Global Offensive"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "boolean",
            "difficulty": "medium",
            "question": "TF2: The Medic will be credited for an assist if he heals a Spy that successfully saps a building.",
            "correct_answer": "True",
            "incorrect_answers": [
                "False"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which of these is NOT the name of a team leader in Pok&eacute;mon GO?",
            "correct_answer": "Leif",
            "incorrect_answers": [
                "Blanche",
                "Spark",
                "Candela"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "In Hitman: Blood Money, what is the name of the target in the mission &quot;Death of a Showman&quot;?",
            "correct_answer": "Joseph Clarence",
            "incorrect_answers": [
                "The Swing King",
                "Maynard John",
                "Manuel Delgado"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which country was Eliza &quot;Ash&quot; Cohen from &quot;Tom Clancy&#039;s Rainbow Six Siege&quot; born in?",
            "correct_answer": "Israel",
            "incorrect_answers": [
                "United States of America",
                "Mexico",
                "Canada"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "In the game Pok&eacute;mon Conquest, which warlord is able to bond with Zekrom and a shiny Rayquazza?",
            "correct_answer": "Nobunaga",
            "incorrect_answers": [
                "The Player",
                "Oichi",
                "Hideyoshi"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which of these games takes place in the Irish town of Doolin, with the option to play as one of the characters, Ellen and Keats?",
            "correct_answer": "Folklore",
            "incorrect_answers": [
                "Shadow of the Colossus",
                "ICO",
                "Beyond Good &amp; Evil"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What are tiny Thwomps called in Super Mario World?",
            "correct_answer": "Thwimps",
            "incorrect_answers": [
                "Little Thwomp",
                "Mini Thwomp",
                "Tiny Tims"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which of the following was not developed by Bethesda?",
            "correct_answer": "Fallout: New Vegas",
            "incorrect_answers": [
                "Fallout 3",
                "The Elder Scrolls V: Skyrim",
                "Fallout 4"
            ]
        },
        {
            "category": "Entertainment: Video Games",
            "type": "multiple",
            "difficulty": "medium",
            "question": "In Call of Duty: Modern Warfare 2, how many consecutive kills does it require to earn the &quot;Tactical Nuke&quot; killstreak?",
            "correct_answer": "25",
            "incorrect_answers": [
                "20",
                "30",
                "35"
            ]
        }
    ]
};